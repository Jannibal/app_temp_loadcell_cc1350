/*
 * delay.h
 *
 *  Created on: 30-01-2018
 *      Author: fondef
 */

#ifndef DELAY_H_
#define DELAY_H_

#include <stdint.h>

extern void delay_us(uint16_t u16timedelay);
extern void Delay1us(void);

#endif /* DELAY_H_ */
