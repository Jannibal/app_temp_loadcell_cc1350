/*
 * delay.c
 *
 *  Created on: 30-01-2018
 *      Author: fondef
 */

#include "delay.h"

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
/****************************************************************************
Nombre:     delay_us
Funci�n:    delay of microseconds.
Devuelve:
****************************************************************************/
void delay_us(uint16_t u16timedelay){

    uint16_t i;

    if(u16timedelay > 1){
       for(i=0; i<u16timedelay; ++i){
           Delay1us();
       }
    }
}

/****************************************************************************
Nombre:     Delay1us
Funci�n:    delay of one microsecond implemented in assembler.
Devuelve:
****************************************************************************/
void Delay1us(void){
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
    __asm (" nop");
}
