/*
 * hx711.c
 *
 *  Created on: 30-01-2018
 *      Author: fondef
 */

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>

#include "hx711.h"
#include "delay.h"

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/
static PIN_Config GPIO_hx711[] =
{
    DOUT  | PIN_INPUT_EN | PIN_NOPULL | PIN_HYSTERESIS, /* DOUT line, output of sensor */
    PD_SCK  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW,   /*PD_SCK, input clock of sensor */
    PIN_TERMINATE                                                                      /* Terminate list */
};

static PIN_State   pinStateHx711;
static PIN_Handle  hPinHx711;

static long ulOffSetLoadCell = 0;

/****************************************************************************/
/***        Funcions exported                                             ***/
/****************************************************************************/
/****************************************************************************
Name: vInit_Hx711
Function: Init hx711 port comunication.
Return:
****************************************************************************/
void vInit_Hx711(void){
    /* Allocate DQ pin */
    hPinHx711 = PIN_open(&pinStateHx711, GPIO_hx711);
    PIN_setPortOutputValue(hPinHx711, GPIO26_LOW);
//    PIN_setPortOutputValue(hPinHx711, GPIO26_HIGH);
}

/****************************************************************************
Name: bHx711isReady
Function: Poll sensor to know if the data is ready.
Return:
****************************************************************************/
bool bHx711isReady(void)
{
    return (PIN_getInputValue(DOUT) == false);
}

/****************************************************************************
Name: lHx711Read
Function: Poll sensor to know if the data is ready.
Return: ADC read
****************************************************************************/
long lHx711Read (void)
{

    long ulValue = 0;
//    long ulPulse = 0;
    uint8_t u8xCount;

    PIN_setPortOutputValue(hPinHx711, GPIO26_LOW);

    while(!bHx711isReady());

//    ulPulse = GPIO26_HIGH;

    for(u8xCount = 0; u8xCount < 24; u8xCount++)
    {
        PIN_setPortOutputValue(hPinHx711, GPIO26_HIGH);
        ulValue = ulValue << 1;
        PIN_setPortOutputValue(hPinHx711, GPIO26_LOW);
//        ulPulse = ~ulPulse;
        ulValue |= PIN_getInputValue(DOUT) & 0x01;

    }

    PIN_setPortOutputValue(hPinHx711, GPIO26_HIGH);
    ulValue ^= 0x800000;
    PIN_setPortOutputValue(hPinHx711, GPIO26_LOW);

//    PIN_setPortOutputValue(hPinHx711, GPIO26_HIGH); /* Power down*/


    return (ulValue) ;
}
/****************************************************************************
Name: vSetOffset
Function: Set the offset of the scale.
Return:
****************************************************************************/
void vSetOffset(void)
{
    ulOffSetLoadCell = lHx711Read_average(10);
}

/****************************************************************************
Name: lHx711scales
Function: read the weight in grams.
Return: grams
****************************************************************************/
float lHx711scale (void)
{
    long ulWeight = lHx711Read_average(1);
    ulWeight -= ulOffSetLoadCell;
    ulWeight *= 1000;   /* scale factor to convert to grams*/

    return ((float) ulWeight / SCALE_FACTOR);
}

/****************************************************************************
Name: lHx711scales
Function: read the weight in grams.
Return: grams
****************************************************************************/
long lHx711Read_average(uint8_t u8N_Times)
{
    long sum = 0;
    uint8_t u8N_Samples;

    for(u8N_Samples = 0; u8N_Samples < u8N_Times; u8N_Samples++)
    {
        sum += lHx711Read();
    }

    return (sum / u8N_Times);
}
/****************************************************************************
Name: vPowerHx711
Function: Change power mode of hx711.
Return:
****************************************************************************/
void vPowerHx711(bool bState)
{
    if( bState)
        PIN_setPortOutputValue(hPinHx711, GPIO26_LOW);
    else
        PIN_setPortOutputValue(hPinHx711, GPIO26_HIGH);
}
