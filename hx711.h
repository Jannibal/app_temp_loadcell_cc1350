/*
 * hx711.h
 *
 *  Created on: 30-01-2018
 *      Author: fondef
 */

#ifndef HX711_H_
#define HX711_H_

/* Example/Board Header files */
#include "Board.h"

#define GPIO26_HIGH   ((1UL) << 26)
#define GPIO26_LOW  ~GPIO26_HIGH
#define DOUT        Board_DIO25_ANALOG
#define PD_SCK      Board_DIO26_ANALOG
#define GAINHX711   ((25*2))
#define SCALE_FACTOR    (-225125)    /* to get read scale in grams.*/

extern void vInit_Hx711(void);
extern long lHx711Read (void);
extern bool bHx711isReady(void);
extern float lHx711scale(void);
extern void vSetOffset(void);
extern long lHx711Read_average(uint8_t u8N_Times);
extern void vPowerHx711(bool bState);


#endif /* HX711_H_ */
