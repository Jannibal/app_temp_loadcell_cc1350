/*
 * OneWire.h
 *
 *  Created on: 09-01-2018
 *      Author: fondef
 */

#ifndef ONEWIRE_H_
#define ONEWIRE_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#define GPIO23_HIGH    ((1UL << 23))
#define GPIO23_LOW  ~GPIO23_HIGH

#define BIT_RES_12   0x7F   // Convertion time 750ms
#define BIT_RES_11   0x5F   // Convertion time 375ms
#define BIT_RES_10   0x3F   // Convertion time 187.5ms
#define BIT_RES_9    0x1F   // Convertion time 93.75ms

extern void vInit_OneWire(void);
extern bool Reset_Pulse(void);
extern void vSetDs18b20(uint8_t u8SetResolution);
extern void vConvertTemp(void);
extern uint16_t vReadTemp(const uint8_t u64Rom_Address[8]);
void vReadRom(uint8_t * u8Address);


#endif /* ONEWIRE_H_ */
