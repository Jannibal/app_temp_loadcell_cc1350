/*
 * Copyright (c) 2016-2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== pinStandby.c ========
 */
#include <unistd.h>

/* Driver Header files */
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>

#include <ti/sysbios/knl/Clock.h>

#include <ti/display/Display.h>

/* Example/Board Header files */
#include "Board.h"

#include "OneWire.h"
#include "hx711.h"

/* Led pin table */
PIN_Config LedPinTable[] =
{
    Board_PIN_LED0  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, /* LED initially off */
    Board_PIN_LED1      | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, /* LED initially off */
    PIN_TERMINATE                                                                      /* Terminate list */
};

uint8_t sensorTemp1[8] = {0x28,0xE0,0xAD,0x30,0x05,0x00,0x00,0x95};

uint8_t sensorTemp2[8] = {0x28,0xF6,0x6A,0xD7,0x03,0x00,0x00,0x11};

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    PIN_State   pinState;
    PIN_Handle  hPin;
    uint_t      currentOutputVal;

    /* Shut down external flash on LaunchPads. It is powered on by default
     * but can be shut down through SPI
     */
    #ifdef Board_shutDownExtFlash
        Board_shutDownExtFlash();
    #endif

    /* Open Display Driver */
    Display_Handle    displayHandle;
    Display_Params    displayParams;
    Display_Params_init(&displayParams);
    displayHandle = Display_open(Display_Type_UART, NULL);

    /* Allocate LED pins */
    hPin = PIN_open(&pinState, LedPinTable);

    vInit_OneWire();
    vInit_Hx711();

    vSetDs18b20(BIT_RES_10);

    /*
     * Repeatedly sleeps for a duration, to allow repeated entry/exit
     * from standby. The LED states are toggled on each iteration
     */

    vSetOffset();
//    vSetOffset();

    while(1) {
        /* Sleep, to let the power policy transition the device to standby */
//        usleep(standbyDuration);

//        time = Clock_getTicks();
//
//        while((time + 1 ) > Clock_getTicks());

//        Reset_Pulse();

//        vReadRom(sensorTemp1);
//
//        Display_printf(displayHandle, 1, 0, "Add: %x ", sensorTemp1[0]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[1]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[2]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[3]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[4]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[5]);
//        Display_printf(displayHandle, 1, 0, "%x ", sensorTemp1[6]);
//        Display_printf(displayHandle, 1, 0, "%x \n", sensorTemp1[7]);

        vConvertTemp();

        sleep(2);





//        if(Reset_Pulse())
//            Display_printf(displayHandle, 1, 0, " Ds18b20 detected \n");

        uint16_t aux_temp = vReadTemp(sensorTemp1);
//        uint16_t aux_temp = vReadTemp(BROADCAST);


        float  Temp = 0;
//        uint16_t aun_Temp = ((uint16_t)((T_MSB << 8) | T_LSB));
        Temp =  (float) aux_temp / 16;
//
//        T_H = ((T_MSB&7)<<4)|(T_LSB&0xF0>>4);
//        T_L = T_LSB&0xF;

        Display_printf(displayHandle, 1, 0, "Raw = %x \n", aux_temp);
        Display_printf(displayHandle, 1, 0, "Temp1 = %f \n", Temp);

        aux_temp = vReadTemp(sensorTemp2);
        Temp =  (float) aux_temp / 16;
        Display_printf(displayHandle, 1, 0, "Temp2 = %f \n", Temp);


       float ulweight = lHx711scale();
//       long ulread = lHx711Read();
//       long average = lHx711Read_average(10);

       Display_printf(displayHandle, 1, 0, "scale = %f\n", ulweight);
//       Display_printf(displayHandle, 1, 0, "Read   = %d\n", ulread);
//       Display_printf(displayHandle, 1, 0, "average   = %i\n", average);

        /* Read current output value for all pins */
        currentOutputVal =  PIN_getPortOutputValue(hPin);

//        Display_printf(displayHandle, 1, 0, "%lu\n", currentOutputVal);

        /* Toggle the LEDs, configuring all LEDs at once */
        PIN_setPortOutputValue(hPin, ~currentOutputVal);
    }
}
